import React, { FC } from 'react';

import { Article } from '../article/article.component';
import { FeedArticle } from '../../api/dto/global-feed.in';

interface ArticleListProps {
  articles: FeedArticle[];
}

export const ArticleList: FC<ArticleListProps> = ({ articles }) => {
  const renderArticle = () =>
    articles.map(a => (
      <Article
        key={a.slug}
        {...a}
      />
    ));

  return (
    <div>
      {renderArticle()}
    </div>
  );
};

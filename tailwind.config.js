/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        titillium: ['Titillium Web', 'sans-serif'],
      },
      colors: {
        conduit: {
          green: '#5CB85C',
          lightenGray: '#ddd',
          darkGreen: '#3d8b3d',
          gray: '#bbb',
          darkestGray: '#373a3c',
          tag: '#aaa',
          darkenGray: '#999',
          pageHoverBg: '#eceeef',
        }
      },
      spacing: {
        navItem: '0.425rem',
        0.3: '0.3rem',
        0.2: '0.2rem',
        tag: '0.6rem',
      },
      boxShadow: {
        conduit: {
          banner: 'inset 0 8px 8px -8px rgb(0 0 0 / 30%), inset 0 -8px 8px -8px rgb(0 0 0 / 30%)',
        }
      },
      dropShadow: {
        logo: '0 1px 3px rgb(0 0 0 / 30%)'
      },
      fontSize: {
        logo: '3.5rem',
        date: '0.8rem',
      },
      borderRadius: {
        buttonSm: '0.2rem',
        tag: '10rem',
      }
    },
  },
  plugins: [],
}

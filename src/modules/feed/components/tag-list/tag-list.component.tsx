import React, { FC } from 'react';

interface TagListProps {
  tagList: string[];
}

export const TagList: FC<TagListProps> = ({ tagList }) => {
  const renderTag = () =>
    tagList.map((t, i) => (
      <li key={i} className='font-light text-date border border-conduit-lightenGray text-conduit-tag mr-1 mb-0.2 px-tag rounded-tag'>
        {t}
      </li>));

  return (
    <ul className='flex'>
      {renderTag()}
    </ul>
  );
};

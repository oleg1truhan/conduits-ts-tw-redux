import React, {
  FC,
  useState
} from 'react';

import { Container } from '../../../../common/components/container/container.component';
import { useGetGlobalFeedQuery } from '../../api/repository';
import { ArticleList } from '../article-list/article-list.component';
import { FeedToggle } from '../feed-toggle/feed-toggle.component';
import ReactPaginate from 'react-paginate';
import { FEED_PAGE_SIZE } from '../../consts';
import { useSearchParams } from 'react-router-dom';
import { serializeSearchParams } from '../../../../utils/router';

interface FeedProps {}

export const Feed: FC<FeedProps> = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [ page, setPage ] = useState(searchParams.get('page') ? Number(searchParams.get('page')) : 0);
  const handlePageChange = ({ selected }: { selected: number }) => {
    setPage(selected);
    setSearchParams(serializeSearchParams({ page: String(selected) }));
  };

  const { data, error, isLoading, isFetching } = useGetGlobalFeedQuery({ page });

  if (isLoading || isFetching) {
    return <Container>
      Feed loading...
    </Container>;
  }

  if (error) {
    return <Container>
      Some error occurred
    </Container>;
  }

  const pageCount = data?.articlesCount || 0 / FEED_PAGE_SIZE;

  return (
    <Container>
      <FeedToggle/>
      <div className='flex'>
        <div className='w-3/4'>
          <ArticleList articles={data?.articles || []}/>
          <nav className='my-6'>
            <ReactPaginate
              pageCount={pageCount}
              previousLabel={null}
              nextLabel={null}
              containerClassName='flex'
              pageClassName='group'
              pageLinkClassName='p-3 text-conduit-green bg-white border border-conduit-lightenGray -ml-px hover:bg-conduit-pageHoverBg group-[&:nth-child(2)]:rounded-l group-[&:nth-last-child(2)]:rounded-r]'
              activeClassName='active group'
              activeLinkClassName='group-[.active]:bg-conduit-green group-[.active]:text-white group-[.active]:border-conduit-green'
              onPageChange={handlePageChange}
              forcePage={page}
            />
          </nav>
        </div>
        <div className='w-1/4'>tags</div>
      </div>
    </Container>
  );
};

import React from 'react';

import { Header } from './common/components/header/header.component';
import { Banner } from './common/components/banner/banner.component';
import { Feed } from './modules/feed/components/feed/feed.component';

export const App = () => {
  return (
    <div className="App">
      <Header/>
      <Banner/>
      <Feed/>
    </div>
  );
};
